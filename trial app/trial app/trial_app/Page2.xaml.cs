﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace trial_app
{
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }
        async void SecondButton(object sender, System.EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
        async void FirstButton(object sender, System.EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}
