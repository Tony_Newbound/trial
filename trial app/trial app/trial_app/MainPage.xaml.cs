﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace trial_app
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        async void FirstButton(object sender, System.EventArgs e)
        {
            await Navigation.PushModalAsync(new Page1());

        }
        async void SecondButton(object sender, System.EventArgs e)
        {
            await Navigation.PushModalAsync(new Page2());
        }

    }
}
